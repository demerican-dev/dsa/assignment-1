
import ballerina/graphql;

public type Covid record {|
    readonly string region;
    string date;
    int confirmed_cases?;
    int deaths?;
    int recoveries?;
    int tested?;
|};

table<CovidEntry> key(region) covidEntriesTable = table [
    {date: "01/09/2020", region: "Khomas", confirmed_cases: 465, deaths: 39, recoveries: 67, tested: 1200},
    {date: "31/08/2020", region: "Hardap", confirmed_cases: 389, deaths: 30, recoveries: 57, tested: 746},
    {date: "28/08/2020", region: "Ojti", confirmed_cases: 541, deaths: 41, recoveries: 43, tested: 897}
];




public distinct service class CovidData {
    private final readonly & Covid entryRecord;

    function init(Covid entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
    }

    resource function get date() returns string? {
        return self.entryRecord.date;
    }

    isolated resource function get region() returns string {
        return self.entryRecord.region;
    }

    isolated resource function get confirmed_cases() returns int? {
        if self.entryRecord.confirmed_cases is int {
            return self.entryRecord.confirmed_cases / 1000;
        }
        return;
    }

    isolated resource function get deaths() returns int? {
        if self.entryRecord.deaths is int {
            return self.entryRecord.deaths / 1000;
        }
        return;
    }

    isolated resource function get recoveries() returns int? {
        if self.entryRecord.recoveries is int {
            return self.entryRecord.recoveries / 1000;
        }
        return;
    }

    isolated resource function get tested() returns int? {
        if self.entryRecord.tested is int {
            return self.entryRecord.tested;
        }
        return;
    }
}


isolated service /Problem2 on new graphql:Listener(9000) {
resource function get all() returns CovidData[] {
        CovidEntry[] covidEntries = covidEntriesTable.toArray().cloneReadOnly();
        return covidEntries.map(entry => new CovidData(entry));
    }

resource function get filter(string region) returns CovidData? {
        CovidEntry? covidEntry = covidEntriesTable[region];
        if covidEntry is CovidEntry {
            return new (covidEntry);
        }
        return;
    }
    
remote function add(CovidEntry entry) returns CovidData {
        covidEntriesTable.add(entry);
        return new CovidData(entry);
    }

}